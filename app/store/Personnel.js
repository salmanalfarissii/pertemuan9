Ext.define('Pertemuan9.store.Personnel', {
    extend: 'Ext.data.Store',

    alias: 'store.personnel',
    storeId:'personnel',
    autoLoad: true,
    autoSync: true,
    fields: [
        'name', 'email', 'phone'
    ],

      proxy: {
        type: 'jsonp',
        api: {
            read: "http://localhost:8080/MyApp_php/readPersonnel.php",
            update: "http://localhost:8080/MyApp_php/updatePersonnel.php",
            destroy: "http://localhost:8080/MyApp_php/destroyPersonnel.php",
            create: "http://localhost:8080/MyApp_php/createPersonnel.php"
        },
        reader: {
            type: 'json',
            rootProperty: 'items' 
        }
    }
    // ,
    // listeners:{
    //     beforeload:function(store,operation,e0pts){
    //         this.getProxy().setExtraParams({
    //             user_id:-1
    //         });
    //     }
    // }
});