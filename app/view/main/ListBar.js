Ext.define('Pertemuan9.view.main.ListBar', {
    extend: 'Ext.grid.Grid',
    xtype: 'listbar',

    requires: [
        'Pertemuan9.store.Personnel',
        'Ext.grid.plugin.Editable'
    ],
    plugins: [{
        type: 'grideditable'
    }], 

    title: 'List Bar',

    bind:'{bar}',
    viewModel:{
        stores:{
            bar:{
                type :'bar',
            }
        }
    },
    columns: [
        { text: 'Name',  dataIndex: 'name', width: 250,  editable:true},
        { text: 'g1', dataIndex: 'g1', width: 250, editable:true},
        { text: 'g2', dataIndex: 'g2', width: 250, editable:true},
        { text: 'g3', dataIndex: 'g3', width: 250, editable:true},
        { text: 'g4', dataIndex: 'g4', width: 250, editable:true},
        { text: 'g5', dataIndex: 'g5', width: 250, editable:true},
        { text: 'g6', dataIndex: 'g6', width: 250, editable:true}
    ],

    listeners: {
        select: 'onBarItemSelected'
    }
});